import { Message } from "./message/Message";

import "./messageList.scss";

const messages = [
  {
    id: 1,
    user: {
      name: "vverbitskiy",
    },
    text: "If you leave the channel, it will disappear",
    time: "12:56",
  },
  {
    id: 2,
    user: {
      name: "bogdanhpk",
    },
    text: "Does 12:03 work?",
    time: "12:53",
  },
  {
    id: 3,
    user: {
      name: "ahorunzhina",
    },
    text: "This channel is not visible to anyone, to start communicating invite the user or copy the link and send it to the interlocutor. ",
    time: "12:51",
  },
];

export function MessageList() {
  return (
    <section className="message_list">
      {messages.map((item) => (
        <Message
          key={item.id}
          name={item.user.name}
          text={item.text}
          time={item.time}
        />
      ))}
    </section>
  );
}

export default MessageList;
