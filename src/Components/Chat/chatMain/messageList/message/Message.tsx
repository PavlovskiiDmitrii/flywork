import "./message.scss";

interface IMessageProps {
    name: string,
    text: string,
    time: string
}

export function Message({name, text, time} : IMessageProps) {
  return (
    <section className="message_wrap">
        <section className="message_avatar_wrap">
            <div className="message_avatar">

            </div>
        </section>
        <div className="message_body">
            <div className="message_user">{name}</div>
            <div className="message_time">{time}</div>
            <div className="message_text">{text}</div>
        </div>
    </section>
  );
}

export default Message;