import {MessageList} from './messageList/MessageList'
import {ChatInput} from './chatInput/ChatInput'

import "./chatMain.scss";

export function ChatMain() {
  return (
    <div className="chat_main">
        <MessageList/>
        <ChatInput/>
    </div>
  );
}

export default ChatMain;