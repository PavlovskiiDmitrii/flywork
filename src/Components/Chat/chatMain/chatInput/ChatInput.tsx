import {Input} from '../../../Ui/Input/input'

import "./chatInput.scss";

export function ChatInput() {
  return (
    <section className="chat_input">
      <Input />
    </section>
  );
}

export default ChatInput;
