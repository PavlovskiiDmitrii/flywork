import { ChatMain } from "./chatMain/ChatMain";
import { ChatHeader } from "./chatHeader/ChatHeader";

import "./chat.scss";

export function Chat() {
  return (
    <div className="chat">
      <ChatHeader />
      <ChatMain />
    </div>
  );
}

export default Chat;
