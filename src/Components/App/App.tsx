import { Chat } from '../Chat';

import './App.scss';

export function App() {
  return (
    <section className="app">
      <section className="leftSide_wrap">

      </section>
      <section className="mainSide_wrap">
        <section className="leftList_wrap"></section>
        <section className="chat_wrap">
          <Chat/>
        </section>
      </section>
    </section>
  );
}

export default App;

